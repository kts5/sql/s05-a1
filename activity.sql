SELECT customerName FROM customers
	WHERE country = 'Philippines';

SELECT contactLastName, contactFirstName
	FROM customers
	WHERE customerName = 'La Rochelle Gifts';

SELECT productName, MSRP FROM products
	WHERE productName = 'The Titanic';

SELECT firstName, lastName FROM employees
	WHERE email = 'jfirrelli@classicmodelcars.com';

SELECT customerName FROM customers
	WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees
	WHERE lastName = 'Patterson' AND firstName = 'Steve';

SELECT customerName, country, creditLimit FROM customers
	WHERE country != 'USA' AND creditLimit > 3000;

SELECT customerNumber FROM orders
	WHERE comments LIKE "%DHL%";

SELECT productLine FROM productLines
	WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers
	WHERE country IN ('USA', 'France', 'Canada');

SELECT firstName, lastName, city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE city = 'Tokyo';

SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE lastName = 'Thompson' AND firstName = 'Leslie';

SELECT productName, customerName FROM products
	JOIN orderDetails ON products.productCode = orderDetails.productCode
	JOIN orders ON orderDetails.orderNumber = orders.orderNumber
	JOIN customers ON orders.customerNumber = customers.customerNumber
	WHERE customerName = 'Baane Mini Imports';
	
SELECT firstName, lastName, customerName, customers.country FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	WHERE customers.country = offices.country;

SELECT productName, quantityInStock FROM products
	WHERE productLine = 'Planes' AND quantityInStock < 1000;

SELECT customerName, phone FROM customers
	WHERE phone LIKE "+81%";